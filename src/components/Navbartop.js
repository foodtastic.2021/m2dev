import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Offcanvas from 'react-bootstrap/Offcanvas';
import { NavLink } from 'react-router-dom';
import './focus';

function Navbartop() {
    return (
        <>
            {['xxl'].map((expand) => (
                <Navbar key={expand} bg="light" expand={expand} className="mb-3">
                    <Container fluid>
                        <Navbar.Brand href="#" style={{}}>M2DEV</Navbar.Brand>
                        <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
                        <Navbar.Offcanvas
                            id={`offcanvasNavbar-expand-${expand}`}
                            aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
                            placement="end"
                        >
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`} style={{ 'margin-left': '200px' }}>

                                </Offcanvas.Title>
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <Nav className="justify-content-end flex-grow-1" style={{ 'left': '250px', 'position': 'absolute', 'top': '8px' }} >
                                    <Nav.Link href="dashboard">Dashboard</Nav.Link>
                                    <Nav.Link href="settings" >Settings</Nav.Link>
                                    <Nav.Link href="profile">Profile</Nav.Link>
                                   
                                </Nav>
                                <Form className="d-flex" style={{ 'right': '250px', 'top': '8px', 'position': 'absolute' }} >
                                    <Form.Control
                                        type="search"
                                        placeholder="Search"
                                        className="me-2"
                                        aria-label="Search"
                                        id="searchid"

                                    />
                                    <Button variant="outline-success">Search</Button>
                                </Form>
                                <Navbar></Navbar>
                            </Offcanvas.Body>
                        </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            ))}
        </>
    );
}

export default Navbartop;                